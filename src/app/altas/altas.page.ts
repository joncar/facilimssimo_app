import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { AltasService } from '../services/altas.service';
import { UserService } from '../services/user.service';
import { ModalController,Events } from '@ionic/angular';
import { getNSSModalPage } from '../modals/getNSS.modal.page';
import { RegistroModalPage } from '../modals/registro.modal.page';
declare var Keyboard:any;
@Component({
  selector: 'app-altas',
  templateUrl: 'altas.page.html'  
})
export class AltasPage {	
	constructor(
	    public router: Router,
	    public app: AppComponent,
	    public api: ApiService, 
      public msj: MessagesService,
      public altas: AltasService,
      public user: UserService,
      public modalController: ModalController,
      public event: Events
	){ 
    altas.data.patronos_id = user.user.id;
    /*if(typeof this.user.user.nombre=='undefined' || this.user.user.nombre == ''){
        this.showModal();
    } */                       
    //Keyboard.automaticScrollToTopOnHiding = true;
	}
        
  modal = undefined;
  async showModal() {
	    this.modal = await this.modalController.create({
	      component: RegistroModalPage,
	      componentProps: {}
	    });
	    let m = this.modal;
	    this.modal.present();
	}

	getCURP(){
    var l = this;
    this.msj.loading('Consultando CURP');
    this.api.get('getCURP',this.altas.data,function(response){
            if(response.success){
                    l.altas.data.curp = response.msj;
                    l.event.publish('updateScreen');
            }else{
                     //Mostrar mensaje de error
              l.msj.alert('ERROR',response.msj);		          
            }
            l.msj.closeLoading();
    });
	}
        
  async getNSS() {
	    this.modal = await this.modalController.create({
	      component: getNSSModalPage,
	      componentProps: {alta:this.altas.data}
	    });
	    let m = this.modal;
	    this.modal.present();
	}
}
