import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
let red = typeof(localStorage.user)=='undefined'?'home':'main';
const routes: Routes = [
  {
    path: '',
    redirectTo: red,
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'registro',
    loadChildren: './registro/registro.module#RegistroPageModule'
  },
  {
    path: 'recover',
    loadChildren: './recover/recover.module#RecoverPageModule'
  },
  {
    path: 'perfil',
    loadChildren: './perfil/perfil.module#PerfilPageModule'
  },
  {
    path: 'main',
    loadChildren: './main/main.module#MainPageModule'
  },
  {
    path: 'altas',
    loadChildren: './altas/altas.module#AltasPageModule'
  },
  {
    path: 'cotizar',
    loadChildren: './cotizar/cotizar.module#CotizarPageModule'
  },
  {
    path: 'contratar',
    loadChildren: './contratar/contratar.module#ContratarPageModule'
  },
  {
    path: 'bajas',
    loadChildren: './bajas/bajas.module#BajasPageModule'
  },
  {
    path: 'estado_cuenta',
    loadChildren: './estado_cuenta/estado_cuenta.module#Estado_cuentaPageModule'
  },
  {
    path: 'terminos',
    loadChildren: './terminos/terminos.module#TerminosPageModule'
  }

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,
    useHash:true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
