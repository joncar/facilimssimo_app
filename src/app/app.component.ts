import { Component } from '@angular/core';

import { Platform,Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router,
    public event: Events,
    public user: UserService
  ) {
    this.initializeApp();
  }

  menuShow = '';
  toggle(){
    this.menuShow=this.menuShow==''?'active-menu':'';
    this.event.publish('updateScreen');
  }

  go(url){
    this.toggle();
    this.router.navigateByUrl(url);
  }

  closeSession(){
    this.user.clean();
    this.toggle();
    this.router.navigateByUrl('home');
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
}
