import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { TerminosModalPage } from './modals/terminos.modal.page';
import { TarjetaModalPage } from './modals/tarjeta.modal.page';
import { RegistroModalPage } from './modals/registro.modal.page';
import { PlanModalPage } from './modals/plan.modal.page';
import { getNSSModalPage } from './modals/getNSS.modal.page';
import { AltasModalPage } from './modals/altas.modal.page';

import { ApiService } from './services/api.service';
import { UserService } from './services/user.service';
import { MessagesService } from './services/messages.service';
import { AltasService } from './services/altas.service';

import { FormsModule } from '@angular/forms';
@NgModule({
  declarations: [AppComponent,TerminosModalPage, TarjetaModalPage, RegistroModalPage,PlanModalPage,getNSSModalPage,AltasModalPage],
  entryComponents: [TerminosModalPage, TarjetaModalPage, RegistroModalPage,PlanModalPage,getNSSModalPage,AltasModalPage], 
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    UserService,
    MessagesService,
    AltasService,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
