import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { AltasService } from '../services/altas.service';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-bajas',
  templateUrl: 'bajas.page.html'  
})
export class BajasPage {
  empleados = [];
  empleadosOriginal = [];
  data = {};  
  filtro = '';
	constructor(
	    public router: Router,
	    public app: AppComponent,
      public api: ApiService,       
      public msj: MessagesService,
      public altas: AltasService,
      public user: UserService
	  ) {
      this.list();
	  }

	activeM = -1;
  toggle(x){
    	let activeM = this.activeM==x?-1:x;
      if(activeM==x){
        let l = this;
        this.msj.confirm('Confirmación','¿Estas seguro que deseas realizar esta acción?',function(){
          l.activeM = activeM;
          l.data = {
              empleados_id:l.empleados[x].id,
              asistencia: 0,
              puntualidad: 0,
              limpieza: 0,
              desempeno: 0
          }
        });      	
      }else{
        this.activeM = activeM;
      }

  }

  list(){
    let l = this;
    this.api.get('empleados',{patronos_id:this.user.user.id},function(response){
      l.empleados = response;  
      l.empleadosOriginal = response;
      console.log(response);
    });
  }

  bajar(){
    let l = this;
    this.msj.confirm('Confirmación','¿Estas seguro que deseas realizar esta acción?',function(){
      l.api.get('empleados/update',l.data,function(response){
        if(response.success){
          l.msj.alert('COMPLETADO',response.msj);
          l.list();
          l.toggle(0);
        }else{
          l.msj.alert('ERROR',response.msj);
        }
      });
    });
  }

  filterItems() {
    let searchTerm = this.filtro;
    if(this.filtro.length>0){
      this.empleados = this.empleadosOriginal.filter(item => {
        return item.apellido.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1 || item.nombre.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1;
      });
    }else{
      this.empleados = this.empleadosOriginal;
    }
  }
}
