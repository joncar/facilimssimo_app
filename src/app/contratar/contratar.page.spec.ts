import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContratarPage } from './contratar.page';

describe('ContratarPage', () => {
  let component: ContratarPage;
  let fixture: ComponentFixture<ContratarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContratarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContratarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
