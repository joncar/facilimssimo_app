import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { AltasService } from '../services/altas.service';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { ModalController } from '@ionic/angular';
import { TerminosModalPage } from '../modals/terminos.modal.page';
@Component({
  selector: 'app-contratar',
  templateUrl: 'contratar.page.html'
})
export class ContratarPage {
	data = {};
	yearValues = '';
	stateButton = 'CONTRATAR';
                 modal = undefined;
                 parametros = [];
	constructor(
	    public router: Router,
	    public app: AppComponent,
	    public altas: AltasService,
	    public api: ApiService,
	    public msj: MessagesService,
                     public modalController: ModalController,
	){
		let year = new Date();
		let y = year.getFullYear();
		let l = [];

		for(let i = y; i<(y+10);i++){			
			l.push(i);
		}
		this.yearValues = l.join(',');
                
                                 this.list();
	}
        
                  list(){
                    let l = this;
                    this.api.get('parametros',{},function(response){
                            l.parametros = response;
                    });
	}

	contratar(){
                        let l = this;
                        this.stateButton = 'CARGANDO...';
                        this.msj.loading('Registrando empleado e intentando realizar su pago');
                        this.api.get('contratar',this.altas.data,function(response){
                                l.stateButton = 'CONTRATAR';
                                if(response.success){
	          //Usuario registrado
	          l.msj.alert('COMPLETADO',response.msj);
	          l.altas.clean();
	          l.router.navigateByUrl('/main');	  
	          l.msj.closeLoading();        
	        }else{
	          //Mostrar mensaje de error
	          l.msj.alert('ERROR',response.msj);
	          l.msj.closeLoading();
	        }
		});
	}
        
         maskString() {
            let inputTxt = this.altas.data.tarjeta_numero;
            inputTxt = inputTxt.replace(/\D/g, "");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            this.altas.data.tarjeta_numero = inputTxt;
        }
        
        async showTermino(id) {
            this.modal = await this.modalController.create({
              component: TerminosModalPage,
              componentProps: {
                text:this.parametros[id]
              }
            });
            let m = this.modal;
            this.modal.present();
        }
}
