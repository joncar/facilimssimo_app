import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { AltasService } from '../services/altas.service';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import { AlertController } from '@ionic/angular';
import { ModalController } from '@ionic/angular';
import { PlanModalPage } from '../modals/plan.modal.page';
import { MessagesService } from '../services/messages.service';
import { RegistroModalPage } from '../modals/registro.modal.page';
@Component({
  selector: 'app-cotizar',
  templateUrl: 'cotizar.page.html'
})
export class CotizarPage {
	planes = [];	
	importe = 0;
    planid = 0;
    modal = undefined;
	constructor(
	    public router: Router,
	    public app: AppComponent,
	    public altas: AltasService,
	    public api: ApiService,
	    public user: UserService,
	    private alertCtrl: AlertController,
	    public modalController: ModalController,
	    public msj: MessagesService
	){
		altas.data.patronos_id = user.user.id		
		this.list();		
	}

	list(){
		let l = this;
		this.api.get('planes',{datos:this.altas.data},function(response){
			l.planes = response;
		});
	}
  	async showModal() {
	    this.modal = await this.modalController.create({
	      component: RegistroModalPage,
	      componentProps: {}
	    });
	    let m = this.modal;
	    this.modal.present();
	}

	selPlan(plan){		
		this.importe = plan.precio;
		this.altas.data.plan_id = plan.id;
		this.altas.data.importe = plan.precio;
		this.altas.data.intervalo = plan.intervalo;
    	this.planid = plan.id;
    	if(this.planid!=1){
    		if(typeof this.user.user.nombre=='undefined' || this.user.user.nombre == ''){
		        this.showModal();
		    }
    	}
	}
                  
  	async preview(id) {
	    this.modal = await this.modalController.create({
	      component: PlanModalPage,
	      componentProps: {
	      	id:id.id
	      }
	    });
	    let m = this.modal;
	    this.modal.present();
	}
        
    async contratar(transaccion){
       if(this.planid!=1){
			if(typeof this.user.user.nombre=='undefined' || this.user.user.nombre == ''){
		        this.showModal();
		        return;
		    }
	   }
       const confirm = await this.alertCtrl.create({
       header: 'Elige una opción',	      
       message: 'Elige una opción para realizar el pago de tu empleado',
                       inputs:[{
                           name:'pago',
                           label:'En efectivo',
                           type:'radio',
                           value:2
                       },{
                           name:'pago',
                           label:'Con tarjeta de crédito',
                           type:'radio',
                           value:1,
                           checked:true
                       }],
	      buttons: [
	      	{
	          text: 'Cerrar',
	          role: 'cancel',
	          cssClass: 'secondary'
            },
           {
	          text: 'Cargar pago ',
	          handler: (data) => {
	            this.altas.data.tipoPago = data;   
                             if(data==1){
                                this.router.navigateByUrl('contratar');                                
                             }else{
                                 this.pay();
                             }
	          }
	        }]
	    });
        await confirm.present();
	}
        
    async descargar(response,link){
          const confirm = await this.alertCtrl.create({
	      header: 'COMPLETADO',	      
	      message: response,                       
	      buttons: [
                           {
	          text: 'Descargar comprobante ',
	          handler: (data) => {
	            document.location.href=link;
	          }
	        },{
	          text: 'Cerrar',
	          role: 'cancel',
	          cssClass: 'secondary'
            }]
	    });
        await confirm.present();
	}
        
	 pay(){
	        let l = this;                        
	        this.msj.loading('Registrando empleado e intentando realizar su pago');
	        this.api.get('contratar',this.altas.data,function(response){                                
	          if(response.success){
	//Usuario registrado
	              l.altas.clean();
	              l.msj.closeLoading();        
	            if(typeof response.link == 'undefined'){
	                l.msj.alert('COMPLETADO',response.msj);
	            }else{
	                l.descargar(response.msj,response.link);
	            }
	            l.router.navigateByUrl('/main');	  
	        }else{
	          //Mostrar mensaje de error
	          l.msj.alert('ERROR',response.msj);
	          l.msj.closeLoading();
	        }
    });
	}
}
