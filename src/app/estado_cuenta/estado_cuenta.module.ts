import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';

import { Estado_cuentaPage } from './estado_cuenta.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild([
      {
        path: '',
        component: Estado_cuentaPage
      }
    ])
  ],
  declarations: [Estado_cuentaPage]
})
export class Estado_cuentaPageModule {}
