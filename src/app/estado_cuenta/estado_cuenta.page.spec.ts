import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Estado_cuentaPage } from './estado_cuenta.page';

describe('Estado_cuentaPage', () => {
  let component: Estado_cuentaPage;
  let fixture: ComponentFixture<Estado_cuentaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Estado_cuentaPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Estado_cuentaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
