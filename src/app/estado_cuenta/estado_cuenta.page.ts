import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import { AlertController } from '@ionic/angular';
import { MessagesService } from '../services/messages.service';
import { ModalController } from '@ionic/angular';
import { TarjetaModalPage } from '../modals/tarjeta.modal.page';
@Component({
  selector: 'app-estado_cuenta',
  templateUrl: 'estado_cuenta.page.html' 
})
export class Estado_cuentaPage {
	transacciones = [];
	total = 0;	
	constructor(
	    public router: Router,
	    public app: AppComponent,
	    public api: ApiService,
	    public user: UserService,
	    public msj: MessagesService,
	    private alertCtrl: AlertController,
	    public modalController: ModalController
	  ) {
	  	this.list();		
	  }

	  list(){
		let l = this;
		this.api.get('transacciones',{patronos_id:this.user.user.id},function(response){
			l.transacciones = response.data;
			l.total = response.total;
		});
	}

	async repay(transaccion){
	 	const confirm = await this.alertCtrl.create({
	      header: 'Elige una opción',	      
	      message: 'Elige una opción para realizar el pago de tu empleado',
	      buttons: [
	      	{
	          text: 'Actualizar tarjeta',
	          handler: () => {
	            this.updateCard(transaccion);
	          }
	        },{
	          text: 'Reintentar',
	          handler: () => {
	            this.retrypay(transaccion);
	          }
	        },{
	          text: 'Cerrar',
	          role: 'cancel',
	          cssClass: 'secondary'
           }]
	    });

		await confirm.present();
	}

	retrypay(transaccion){
		let l = this;
		this.msj.loading('Realizando pago por favor espere...');		
		this.api.get('retryPay',{transacciones_id:transaccion.id},function(response){			
			if(response.success){
	          //Usuario registrado
	          l.msj.alert('COMPLETADO',response.msj);	          
	          l.list();	
	          l.msj.closeLoading();          
	        }else{
	          //Mostrar mensaje de error
	          l.msj.alert('ERROR',response.msj);
	          l.list();	
	          l.msj.closeLoading();          
	        }
		});
	}

	modal = undefined;

	async updateCard(transaccion) {
	    this.modal = await this.modalController.create({
	      component: TarjetaModalPage,
	      componentProps: {
	      	'transaccion':transaccion.id,
	      	'l':this
	      }
	    });
	    let m = this.modal;
	    this.modal.present();
	}
}
