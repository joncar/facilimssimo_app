import { Component,ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { UserService } from '../services/user.service';
import { ModalController } from '@ionic/angular';
import { AltasModalPage } from '../modals/altas.modal.page';
@Component({
  selector: 'app-main',
  templateUrl: 'main.page.html',
  styleUrls: ['main.page.scss'],
})
export class MainPage {
        constructor(
            public router: Router,
            public app: AppComponent,
            public user: UserService,
            public modalController: ModalController
        ) {
            if(!user.isLogin){
                user.clean();
                this.router.navigateByUrl('home');
            }
        }
        
        modal = undefined;
                  
       async alta() {
            this.modal = await this.modalController.create({
              component: AltasModalPage,
              componentProps: {}
            });
            let m = this.modal;
            this.modal.present();
        }
}
