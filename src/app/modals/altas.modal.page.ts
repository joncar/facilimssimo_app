import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';
import { getNSSModalPage } from '../modals/getNSS.modal.page';

@Component({
  selector: 'app-altasModal',
  templateUrl: './altas.html',
})
export class AltasModalPage {    
  constructor(
    public modal : ModalController,
    private navParams: NavParams ,
    public router: Router,   
    public modalController: ModalController,    
  ) {    
  }
  
  modale = undefined;
  async getNSS() {
       this.modale = await this.modalController.create({
         component: getNSSModalPage,
         componentProps: {}
       });
       let m = this.modale;
       this.modale.present();
   }
  
  alta(){
      this.router.navigateByUrl('altas');
      this.close();
  }
    
  close(){
        this.modal.dismiss();
  }
}
