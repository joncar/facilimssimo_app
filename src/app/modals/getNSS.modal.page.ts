import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { AltasService } from '../services/altas.service';

@Component({
  selector: 'app-getNSSModal',
  templateUrl: './getNSS.html',
})
export class getNSSModalPage {  
  id = '';
  data = {email:'',curp:''};
  constructor(
    public modal : ModalController,
    private navParams: NavParams,
    public api: ApiService,
    public altas: AltasService,
  ) {
    this.data = typeof navParams.get('alta') !='undefined'?navParams.get('alta'):this.data;          
  }
  
  ngAfterViewInit(){
      var mapForm = document.createElement("form");
        mapForm.target = "nss";
        mapForm.method = "POST"; // or "post" if appropriate
        mapForm.action = "https://serviciosdigitales.imss.gob.mx/gestionAsegurados-web-externo/asignacionNSS/valida";

        var mapInput = document.createElement("input");
        mapInput.type = "text";
        mapInput.name = "curp";
        mapInput.value = this.data.curp;
        mapForm.appendChild(mapInput);

        mapInput = document.createElement("input");
        mapInput.type = "text";
        mapInput.name = "correoElectronico.correo";
        mapInput.value = this.data.email;
        mapForm.appendChild(mapInput);
        document.body.appendChild(mapForm);

        mapInput = document.createElement("input");
        mapInput.type = "text";
        mapInput.name = "correoElectronicoFiscal.correo";
        mapInput.value = this.data.email;
        mapForm.appendChild(mapInput);
        document.body.appendChild(mapForm);

        mapInput = document.createElement("input");
        mapInput.type = "text";
        mapInput.name = "captcha";
        mapInput.value = '';
        mapForm.appendChild(mapInput);
        document.body.appendChild(mapForm);                        
        mapForm.submit();
        document.body.removeChild(mapForm);
  }
  close(){
        this.modal.dismiss();
  }
}
