import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';
import { ApiService } from '../services/api.service';


@Component({
  selector: 'app-planModal',
  templateUrl: './plan.html',
})
export class PlanModalPage {  
  id = '';
  plan = {};
  constructor(
    public modal : ModalController,
    private navParams: NavParams,
    public api: ApiService
  ) {
    this.id = navParams.get('id');  
    this.list();
  }
  
  list(){
        let l = this;
        this.api.get('ver_plan/'+this.id,{},function(response){
                l.plan = response[0];
        });
  }
  close(){
  	this.modal.dismiss();
  }
}
