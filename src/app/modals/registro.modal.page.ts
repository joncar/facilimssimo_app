import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-registroModal',
  templateUrl: './registro.html',
})
export class RegistroModalPage {    
  data = {id:''}  
  constructor(
    public modal : ModalController,
    private navParams: NavParams,
    public api: ApiService,
    private msj: MessagesService,
    public user: UserService,
  ) {    
  }

  register(){
    this.data.id = this.user.user.id;    
    let l = this;    
    this.msj.loading('Actualizando información');
    this.api.get('actualizarPatron',this.data,function(response){      
      if(response.success){
        //Usuario registrado     
        l.user.save(l.data);           
        l.close();
        l.msj.closeLoading();        
      }else{
        //Mostrar mensaje de error
        l.msj.alert('ERROR',response.msj);
        l.msj.closeLoading();        
      }
    });
  }
  
  close(){
  this.modal.dismiss();
  }
}
