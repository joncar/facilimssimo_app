import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';

@Component({
  selector: 'app-tarjetaModal',
  templateUrl: './tarjeta.html',
})
export class TarjetaModalPage {  
  transaccion = '';
  data = {
    transaccion:'',
    tarjeta_nombre:'',
    tarjeta_numero:'',
    tarjeta_vencimiento:'',
    tarjetacvc:'',
    terminos:''
  }
  l = undefined;
  constructor(
    public modal : ModalController,
    private navParams: NavParams,
    public api: ApiService,
    private msj: MessagesService
  ) {
    this.transaccion = navParams.get('transaccion');
    this.data.transaccion = this.transaccion;  
    this.l = navParams.get('l');  
  }

  contratar(){
    let l = this;    
    this.msj.loading('Actualizando información de pago');
    this.api.get('updateCard',this.data,function(response){      
      if(response.success){
        //Usuario registrado
        l.msj.alert('COMPLETADO',response.msj);                        
        l.msj.closeLoading();
        l.l.retrypay(l.transaccion);
      }else{
        //Mostrar mensaje de error
        l.msj.alert('ERROR',response.msj);
        l.msj.closeLoading();        
      }
    });
  }
  close(){
  	this.modal.dismiss();
  }
  
  maskString() {
            let inputTxt = this.data.tarjeta_numero;
            inputTxt = inputTxt.replace(/\D/g, "");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            inputTxt = inputTxt.replace(/(\d{4})(\d)/, "$1 $2");
            this.data.tarjeta_numero = inputTxt;
        }
}
