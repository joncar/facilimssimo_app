import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';


@Component({
  selector: 'app-terminosModal',
  templateUrl: './terminos.html',
})
export class TerminosModalPage {  
  text = '';
  constructor(
    public modal : ModalController,
    private navParams: NavParams
  ) {
    this.text = navParams.get('text');  
  }
  close(){
  	this.modal.dismiss();
  }
}
