import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { AppComponent } from '../app.component';
@Component({
  selector: 'app-perfil',
  templateUrl: 'perfil.page.html',
})
export class PerfilPage {
  data = {};
  constructor(
    public router: Router, 
    public api: ApiService, 
    public msj: MessagesService,
    public user: UserService,
    public app: AppComponent
  ) {    
    this.data = user.user;       
  }

  

  register(){
    let l = this;    
    this.api.get('registro/1',this.data,function(response){
        if(response.success){
          //Usuario registrado
          l.msj.alert('SUCCESS','Usuario actualizado con éxito');
          l.user.save(l.data);          
        }else{
          //Mostrar mensaje de error
          l.msj.alert('ERROR',response.msj);
        }
    });
  }
}
