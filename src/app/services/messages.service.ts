import { Injectable } from '@angular/core';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class MessagesService {
	loadingItem = undefined;
	constructor(
		private alertCtrl: AlertController,
		private loadingCtrl: LoadingController
	) {
		
	}

	async alert(title,message){
		const alert = await this.alertCtrl.create({
	      header: title,	      
	      message: message,
	      buttons: [{
	      	role:'Cancel',
	      	text:'Cerrar'
	      }]
	    });

		await alert.present();
	}

	async confirm(title,message,okCallback){
		const confirm = await this.alertCtrl.create({
	      header: title,	      
	      message: message,
	      buttons: [{
	          text: 'Cancelar',
	          role: 'cancel',
	          cssClass: 'secondary'
           },{
	          text: 'Aceptar',
	          handler: () => {
	            okCallback();
	          }
	        }]
	    });

		await confirm.present();
	}

	async loading(msj) {		
	    this.loadingItem = await this.loadingCtrl.create({
	      message: msj,
	      duration: 0
	    });
	    return await this.loadingItem.present();	    	    
	}

	async closeLoading() {		
	    this.loadingItem.dismiss();
	}

	/* END GENERICS */
}
