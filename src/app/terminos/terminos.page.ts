import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { TerminosModalPage } from '../modals/terminos.modal.page';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-terminos',
  templateUrl: 'terminos.page.html',  
})
export class TerminosPage {
	parametros = [];
	constructor(
	    public router: Router,
	    public modalController: ModalController,
	    public app: AppComponent,
	    public api: ApiService,
	) {
		this.list();		
	}

	modal = undefined;

	async showTermino(id) {
	    this.modal = await this.modalController.create({
	      component: TerminosModalPage,
	      componentProps: {
	      	text:id
	      }
	    });
	    let m = this.modal;
	    this.modal.present();
	}

	list(){
		let l = this;
		this.api.get('parametros',{},function(response){
			l.parametros = response;
		});
	}
}
